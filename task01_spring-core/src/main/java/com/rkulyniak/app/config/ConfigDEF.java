package com.rkulyniak.app.config;

import com.rkulyniak.app.bean.BeanA;
import com.rkulyniak.app.bean.BeanD;
import com.rkulyniak.app.bean.BeanE;
import com.rkulyniak.app.bean.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@ComponentScan("com.rkulyniak.app.bean")
public class ConfigDEF {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public BeanD beanD() {
        return new BeanD();
    }

    @Bean
    public BeanE beanE1(BeanA beanA1) {
        return new BeanE(beanA1);
    }

    @Bean
    public BeanE beanE2(BeanA beanA2) {
        return new BeanE(beanA2);
    }

    @Bean
    public BeanE beanE3(BeanA beanA3) {
        return new BeanE(beanA3);
    }

    @Bean
    @Lazy
    public BeanF beanF() {
        return new BeanF();
    }
}
