package com.rkulyniak.app.config;

import com.rkulyniak.app.bean.BeanA;
import com.rkulyniak.app.bean.BeanB;
import com.rkulyniak.app.bean.BeanC;
import com.rkulyniak.app.bean.BeanD;
import org.springframework.context.annotation.*;

@Configuration
@Import(ConfigDEF.class)
@PropertySource("bean.properties")
public class ConfigABC {

    @Bean
    @DependsOn({"beanD", "beanB", "beanC"})
    public BeanA beanA1(BeanB beanB, BeanC beanC) {
        BeanA beanA1 = new BeanA();
        beanA1.setBeanB(beanB);
        beanA1.setBeanC(beanC);
        return beanA1;
    }

    @Bean
    @DependsOn("beanA1")
    public BeanA beanA2(BeanB beanB, BeanD beanD) {
        BeanA beanA2 = new BeanA();
        beanA2.setBeanB(beanB);
        beanA2.setBeanD(beanD);
        return beanA2;
    }

    @Bean
    @DependsOn("beanA2")
    public BeanA beanA3(BeanC beanC, BeanD beanD) {
        BeanA beanA3 = new BeanA();
        beanA3.setBeanC(beanC);
        beanA3.setBeanD(beanD);
        return beanA3;
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public BeanB beanB() {
        return new BeanB();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public BeanC beanC() {
        return new BeanC();
    }
}
