package com.rkulyniak.app.exception;

import org.springframework.beans.BeansException;

public class InvalidBeanException extends BeansException {

    public InvalidBeanException(String msg) {
        super(msg);
    }

    public InvalidBeanException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
