package com.rkulyniak.app.bean;

import com.rkulyniak.app.service.BeanValidator;
import lombok.Data;
import org.springframework.beans.factory.BeanNameAware;

@Data
public class BeanF implements BeanValidator, BeanNameAware {

    private String name;
    private int value;

    @Override
    public void setBeanName(String name) {
        this.name = name;
    }

    @Override
    public boolean isBeanValid() {
        return name != null && value > 0;
    }
}
