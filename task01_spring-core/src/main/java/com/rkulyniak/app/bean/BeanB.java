package com.rkulyniak.app.bean;

import com.rkulyniak.app.service.BeanValidator;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
@Data
public class BeanB implements BeanValidator, BeanNameAware {

    private String name;
    private int value;

    @Override
    public void setBeanName(String name) {
        this.name = name;
    }

    @Value("${beanB.value}")
    public void setValue(int value) {
        this.value = value;
    }

    public void init() {
        log.info("BeanB:init");
    }

    public void anotherInit() {
        log.info("BeanB:anotherInit");
    }

    public void destroy() {
        log.info("BeanB:destroy");
    }

    @Override
    public boolean isBeanValid() {
        return name != null && value > 0;
    }
}
