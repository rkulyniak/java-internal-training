package com.rkulyniak.app.bean;

import com.rkulyniak.app.service.BeanValidator;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
@Data
public class BeanA implements BeanValidator, BeanNameAware,
        InitializingBean, DisposableBean {

    private String name;
    private int value;

    private BeanB beanB;
    private BeanC beanC;
    private BeanD beanD;

    @Override
    public void setBeanName(String name) {
        this.name = name;
    }

    @Value("10")
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void afterPropertiesSet() {
        log.info("BeanA:afterPropertiesSet");
    }

    @Override
    public void destroy() {
        log.info("BeanA:destroy");
    }

    @Override
    public boolean isBeanValid() {
        return name != null && value > 0;
    }
}
