package com.rkulyniak.app.bean.infrastucture;

import com.rkulyniak.app.service.BeanValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class BeanPostProcessorImpl implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof BeanValidator) {
            BeanValidator beanValidator = (BeanValidator) bean;
            if (beanValidator.isBeanValid()) {
                log.info("{} is valid", beanName);
            } else {
                log.error("{} isn't valid", beanName);
            }
        } else {
            log.info("{} isn't istanceof BeanValidator", beanName);
        }
        return bean;
    }
}
