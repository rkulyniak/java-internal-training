package com.rkulyniak.app.bean;

import com.rkulyniak.app.service.BeanValidator;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Slf4j
@Data
public class BeanE implements BeanValidator, BeanNameAware {

    private String name;
    private int value;

    private BeanA beanA;

    public BeanE(BeanA beanA) {
        this.beanA = beanA;
    }

    @Override
    public void setBeanName(String name) {
        this.name = name;
    }

    @Value("10")
    public void setValue(int value) {
        this.value = value;
    }

    @PostConstruct
    public void init() {
        log.info("BeanE:PostConstruct");
    }

    @PreDestroy
    public void destroy() {
        log.info("BeanE:PreDestroy");
    }

    @Override
    public boolean isBeanValid() {
        return name != null && value > 0;
    }
}
