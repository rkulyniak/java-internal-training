package com.rkulyniak.app.service;

public interface BeanValidator {

    boolean isBeanValid();
}
