package com.rkulyniak.app;

import com.rkulyniak.app.config.ConfigABC;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

@Slf4j
public class SpringCoreApp {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(ConfigABC.class);
        printInfo(applicationContext);
        applicationContext.close();
    }

    private static void printInfo(BeanDefinitionRegistry beanDefinitionRegistry) {
        String[] beanDefinitionNames = beanDefinitionRegistry.getBeanDefinitionNames();
        System.out.println("applicationContext's beans:");
        Arrays.stream(beanDefinitionNames)
                .forEach(System.out::println);
        System.out.println("applicationContext's user bean definitions");
        Arrays.stream(beanDefinitionNames)
                .forEach(beanName -> System.out.println(beanDefinitionRegistry.getBeanDefinition(beanName)));
    }
}
